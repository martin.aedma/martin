import { useFrame } from "@react-three/fiber";
import React, { useEffect, useRef, useState} from "react";
import { ParticleMaterial } from "./Materials";
import { Vector3 } from "three";

export const Images = (props) => {

    const ref = useRef()
    const meshRef = useRef()
    const [hover, setHover] = useState({in: false, out: false, isMoving: false})

    useEffect(() => {
      ref.current.uTexture = props.texture
    }, [])

    const handlePointerClick = (e) => {
      if(!props.cameraPos.isAnimating){
        props.setCameraPos({
          prev: props.cameraPos.current,
          current: [props.position[0], props.position[1], props.position[2] + 135],
          lookAt: [props.position[0], props.position[1], props.position[2]],
          isAnimating: true,
          view: props.position,
          stateID: props.id
        })
      }
    }

    const handlePointerHoverIn = (e) => {
      if(!props.cameraPos.isAnimating){
        setHover({in: true, out: false, isMoving: true})
      }
    }

    const handlePointerHoverOut = (e) => {
      if(!props.cameraPos.isAnimating){
        setHover({in: false, out: true, isMoving: true})
      }
    }

    let time = 0;

    useFrame(({camera, clock, mouse, size}) => {

        ref.current.uTime = clock.getElapsedTime()
        ref.current.uMouseX = mouse.x / 2 + 0.5
        ref.current.uMouseY = mouse.y / 2 + 0.5
        ref.current.uScreenW = size.width
        ref.current.uScreenH = size.height

        if(hover.isMoving){

        let startRot = new Vector3(0, meshRef.current.rotation.y, 0)
        let lerp = new Vector3(0, 0, 0)
        let endRot = new Vector3(0, 0, 0)
        time += clock.getDelta() * 20 + 0.01

        if(hover.in){
          endRot = new Vector3(0, Math.atan2( ( camera.position.x - meshRef.current.position.x ), ( camera.position.z - meshRef.current.position.z ) ), 0)
        }

        if(hover.out){
          endRot = new Vector3(0, 0, 0)
        }

        lerp.lerpVectors(startRot, endRot, time)
        meshRef.current.rotation.y = lerp.y

        if( time > 1){
          time = 0;
          setHover( p => { return { ...p, isMoving: false}})
        }

      }
    })

    return  <mesh ref={meshRef} position={props.position}
      onPointerDown={handlePointerClick}
      onPointerEnter={handlePointerHoverIn}
      onPointerLeave={handlePointerHoverOut}
      >
      <boxGeometry args={[props.size, props.size, 35]} />
      <meshBasicMaterial attachArray="material" color="gray" />
      <meshBasicMaterial attachArray="material" color="gray" />
      <meshBasicMaterial attachArray="material" color="gray" />
      <meshBasicMaterial attachArray="material" color="gray" />
      <particleMaterial attachArray="material" map={props.t} ref={ref} />
      <meshBasicMaterial attachArray="material" color="yellow" />

    </mesh>
  }