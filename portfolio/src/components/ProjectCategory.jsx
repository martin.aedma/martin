import { useFrame } from "@react-three/fiber";
import React, { useEffect, useRef, useState} from "react";
import { ParticleMaterial } from "./Materials";
import * as THREE from 'three'
import { Vector3 } from "three";

export const ProjectCategory = (props) => {

    const ref = useRef()
    const meshRef = useRef()
    const [hover, setHover] = useState({in: false, out: false, isMoving: false})


    useEffect(() => {
      ref.current.uTexture = props.texture
    }, [])

    const handlePointerClick = (e) => {
      if(!props.projectView.isAnimating){
        props.setProjectView( p => { return { ...p, isAnimating: true, id: props.id}})
      }
    }

    const handlePointerHoverIn = (e) => {

    }

    const handlePointerHoverOut = (e) => {

    }

    let time = 0;

    useFrame(({camera, clock, mouse, size}) => {

      ref.current.uTime = clock.getElapsedTime()
      ref.current.uMouseX = mouse.x / 2 + 0.5
      ref.current.uMouseY = mouse.y / 2 + 0.5
      ref.current.uScreenW = size.width
      ref.current.uScreenH = size.height

      if(props.projectView.isAnimating && props.projectView.id === props.id){
        props.setStateMachine( p => {return { ...p, next:props.id }})
        props.setProjectView( p => { return { ...p, isAnimating: false}})
      }
    })

    return  <mesh ref={meshRef} position={props.position}
      onPointerDown={handlePointerClick}
      onPointerEnter={handlePointerHoverIn}
      onPointerLeave={handlePointerHoverOut}
      >
      <boxGeometry args={[props.size, props.size, 35]} />
      <meshBasicMaterial attachArray="material" color="gray" />
      <meshBasicMaterial attachArray="material" color="gray" />
      <meshBasicMaterial attachArray="material" color="gray" />
      <meshBasicMaterial attachArray="material" color="gray" />
      <particleMaterial attachArray="material" map={props.texture} ref={ref}/>
      <meshBasicMaterial attachArray="material" color="yellow" />
    </mesh>
  }