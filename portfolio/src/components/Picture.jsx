import React, {useMemo, useState, useRef, useEffect} from "react"
import { useFrame } from "@react-three/fiber"

export const Picture = (props) => {

    const [coords, colors, randoms] = useMemo(() => {
      const coords = new Float32Array(props.positions)
      const colors = new Float32Array(props.colors)
      const randoms = new Float32Array(props.randoms)
      return [coords, colors, randoms]
    }, [props])

    const ref = useRef()
    const pointsRef = useRef()

    const [diff, setDiff] = useState(props.sizes.max-props.sizes.min)

    useEffect(() => {
      ref.current.uMinX = props.sizes.min
      ref.current.uMaxX = props.sizes.max
    }, [])

    useFrame(({clock}) => {
      ref.current.uTime = clock.getElapsedTime()
      ref.current.uAnimDist = props.sizes.min + diff * Math.abs(Math.sin(clock.getElapsedTime() * 0.1))
    })

    return  <points position={[0, 0, 0]} ref={pointsRef}>
      < bufferGeometry attach='geometry'>
        <bufferAttribute
          attachObject={["attributes", "position"]}
          count={coords.length/3}
          array={coords}
          itemSize={3}
        />
        <bufferAttribute
          attachObject={["attributes", "uColor"]}
          count={colors.length}
          array={colors}
          itemSize={1}
        />
        <bufferAttribute
          attachObject={["attributes", "uRandoms"]}
          count={randoms.length/3}
          array={randoms}
          itemSize={3}
        />
      </bufferGeometry>
      <particleMaterial ref={ref} />
    </points>
  }