import React, { useState } from "react"
import { Canvas } from '@react-three/fiber'
import { PerspectiveCamera } from '@react-three/drei'
import { Lights } from './Lights'
import { ProjectCategory } from "./ProjectCategory";
import { TextureLoader } from "three";

const Projects = (props) => {

    const [projectView, setProjectview] = useState({isAnimating: false, id: false})
    const texture = new TextureLoader
    const webdev = texture.load('/webdev.svg')
    const csharp = texture.load('/csharp.svg')
    const cplusplus = texture.load('/cplusplus.svg')
    const size = 25

    return <div className="webgl-canvas">
    <Canvas>
      <Lights/>
      {/* <OrbitControls ref = {ref}/> */}
      <ProjectCategory id={'webdev'} texture={webdev} position={[ -10, 15, 0]} size={35} setStateMachine={props.setStateMachine} projectView={projectView} setProjectView={setProjectview} />
      <ProjectCategory id={'csharp'} texture={csharp} position={[ 25, 10, 0]} size={size} setStateMachine={props.setStateMachine} projectView={projectView} setProjectView={setProjectview} />
      <ProjectCategory id={'cplusplus'} texture={cplusplus} position={[ -5, -20, 0]} size={size} setStateMachine={props.setStateMachine} projectView={projectView} setProjectView={setProjectview} />
      <PerspectiveCamera makeDefault position={[0,0,125]}/>
      {/* <AnimateFrame/> */}
    </Canvas>
    </div>
}

export default Projects