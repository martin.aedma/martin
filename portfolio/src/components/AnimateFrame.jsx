import * as THREE from 'three'
import { useFrame } from '@react-three/fiber';

export const AnimateFrame = (props) => {

    let time = 0;
    let isSet = false;

    useFrame(({clock}) => {

        // set inital lookAt
        if(!props.cameraPos.initial){
            props.object.current.lookAt(0, 0, 0)
            props.setCameraPos( p => {return {...p, initial: true }})
            return
        }

        // rotate & move camera

        if(props.object.current) {

            const f = new THREE.Vector3(props.cameraPos.prev[0], props.cameraPos.prev[1], props.cameraPos.prev[2])
            const l = new THREE.Vector3(props.cameraPos.current[0], props.cameraPos.current[1], props.cameraPos.current[2])

            if(f.z !== l.z || f.x !== l.x || f.y !== l.y ){

            const lerp = new THREE.Vector3()
            let newLookAt = new THREE.Vector3(props.cameraPos.view[0], props.cameraPos.view[1], props.cameraPos.view[2]);
            let currentVector = new THREE.Vector3(0, 0, 0);
            const lerpLookAt = new THREE.Vector3()

            if(!isSet){
                time = clock.getElapsedTime()
                isSet = true
            }

            lerp.lerpVectors(f, l, clock.getElapsedTime() - time)
            props.object.current.position.x = lerp.x
            props.object.current.position.y = lerp.y
            props.object.current.position.z = lerp.z

            lerpLookAt.lerpVectors(currentVector, newLookAt, clock.getElapsedTime() - time)
            props.object.current.lookAt(lerpLookAt.x, lerpLookAt.y, lerpLookAt.z)

            if ( isSet && clock.getElapsedTime() > time + 1 ) {
                props.setCameraPos( p => { return {...p, prev : [l.x, l.y, l.z]}})
                isSet = false
                props.setStateMachine( p => {return { ...p, next:props.cameraPos.stateID }})
            }
        }
      }
    })
    return null
  }