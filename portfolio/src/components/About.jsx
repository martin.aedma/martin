import React from "react";

export const About = ({setStateMachine}) => {

    const handleClick = (e) => {
        setStateMachine( p => {return { ...p, next:'home'}})
    }

    return <div className="about-container">

        <div className="about-content-container">

            <div className="about-header">
                <span>MARTIN AEDMA</span>
                <div className="about-links">
                        <a href="https://www.linkedin.com/in/martin-aedma-7380aa1a7/" target={'_blank'} rel="noopener noreferrer"><img src="/linkedin.svg"/></a>
                        <a href="https://www.instagram.com/martinaedma/" target={'_blank'} rel="noopener noreferrer"><img src="/instagram.svg"/></a>
                </div>
            </div>

            <div className="about-text">
                <p> <span>"Hi,</span> I started out as a sport and exercise scientist with
                a PhD from University of Tartu, Estonia. I moved to Finland, Jyväskylä
                in 2015 and started a Brazilian Jiu Jitsu gym - Fx Jiu Jitsu.</p>
            </div>

            <div className="about-text">
                <p>In 2019, at the age of 34, I started to code and study for
                computer science degree @JAMK. I worked as a web front-end developer
                @Liveto. I continue to work on front-end skills while also aiming
                to become full stack. Looking for work as a junior web developer"</p>
            </div>

            <div className="about-footer">
                <span>WEB DEVELOPER</span>
                <div className="btn" style={{zIndex: 2, width:'3rem', height:'3rem'}} onClick={handleClick}>
                    <span>R</span>
                </div>
            </div>
        </div>
    </div>
}