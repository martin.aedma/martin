import React from "react";
import { RectAreaLight } from "three";

export const Lights = () => {
    return (
        <>
            <ambientLight intensity={0.75}/>
            <pointLight intensity={1.0} position={[100, 0, 0]} />
        </>
    )
}