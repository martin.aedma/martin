import React, { useRef, useState } from "react"
import { Canvas } from '@react-three/fiber'
import { PerspectiveCamera } from '@react-three/drei'
import { Lights } from './Lights'
import * as THREE from "three";
import { Images } from "./Images";
import { AnimateFrame } from "./AnimateFrame";

const Animation = (props) => {

  const ref = useRef()
  const cameraRef = useRef()
  const [cameraPos, setCameraPos] = useState({
    prev: [-102.7, 14.6, 246.6],
    current: [-102.7, 14.6, 246.6],
    initial: false,
    rotation: [-0.0193, -0.596, -0.0108],
    lookAt: [0, 0, 0],
    isAnimating: false,
    view: 0,
    stateID: false
  })
  const texture = new THREE.TextureLoader
  const projects = texture.load('/projects.svg')
  const about = texture.load('/about.svg')
  const contact = texture.load('/contact.svg')
  const size = 100

  return <div className="webgl-canvas">
    <Canvas>
      <Lights/>
      {/* <OrbitControls ref = {ref}/> */}
      <Images id={'about'} texture={about} position={[60, -45, -30]} size={size} cameraPos={cameraPos} setCameraPos={setCameraPos}/>
      <Images id={'contact'} texture={contact} position={[-60, -45, -30]} size={size} cameraPos={cameraPos} setCameraPos={setCameraPos}/>
      <Images id={'projects'} texture={projects} position={[0, 65, -30]} size={size} cameraPos={cameraPos} setCameraPos={setCameraPos}/>
      <PerspectiveCamera makeDefault position={cameraPos.prev} ref={cameraRef} />
      <AnimateFrame object={cameraRef} cameraPos={cameraPos} setCameraPos={setCameraPos} orbit={ref} setStateMachine={props.setStateMachine}/>
    </Canvas>
</div>
}

export default Animation

