import React, { useEffect, useState } from "react";
import { WEBDEV, CSHARP, CPLUSPLUS } from "./ContentVariables";
import { About } from "./About";
import dynamic from 'next/dynamic'

const DynamicScene = dynamic(() => import('./Animation'), {ssr: false})
const DynamicProjects = dynamic(() => import('./Projects'), {ssr: false})

export const Portfolio = () => {

    const [stateMachine, setStateMachine] = useState({current:'home', next: false})
    const [containerAnimation, setContainerAnimation] = useState({in: true, done: true})

    useEffect(() => {
        if(stateMachine.next !== false){
            // Animate to switch views
            setContainerAnimation({ in: false, done: false})
        }
    }, [stateMachine])


    const handleAnimationEnd = (e) => {
        if(containerAnimation.done === false){
            setStateMachine( p => { return {...p, current: p.next, next: false }})
            setContainerAnimation({ in: true, out: false, done: true})
        }
    }

    const handleClick = (e) => {
        setStateMachine( p => {return { ...p, next: 'home'}})
    }

    const displayContent = () => {
        switch(stateMachine.current){
            case 'home':
                return <DynamicScene setStateMachine={setStateMachine}/>
            case 'projects':
                return <DynamicProjects setStateMachine={setStateMachine}/>
            case 'webdev':
                return <WEBDEV/>
            case 'csharp':
                return <CSHARP/>
            case 'cplusplus':
                return <CPLUSPLUS/>
            case 'about':
                return <About setStateMachine={setStateMachine}/>
        }
    }

    return <>
        <div className={containerAnimation.in ? 'mainview-container fade-in' : 'mainview-container fade-out'} onAnimationEnd={handleAnimationEnd}>
            {displayContent()}
        </div>
        {stateMachine.current !== 'home' && stateMachine.current !=='about' && <div className={containerAnimation.in ? "return-button-holder fade-in" : "return-button-holder fade-out"}>
            <div className="btn" onClick={handleClick}>
                <span>RETURN</span>
            </div>
        </div>}
    </>
}