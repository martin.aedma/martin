import { shaderMaterial } from "@react-three/drei"
import { extend } from "@react-three/fiber"
import glsl from 'glslify'

export const ParticleMaterial = shaderMaterial(
    {
      uTime : 0,
      uTexture: false,
      uAnimDist: 0,
      uMouseX: 0,
      uMouseY: 0,
      uScreenW: 0,
      uScreenH: 0,
    },
    glsl`
      precision mediump float;

      uniform float uTime;
      uniform float uAnimDist;
      uniform float uMouseX;
      uniform float uMouseY;
      uniform float uScreenW;
      uniform float uScreenH;

      varying float uCol;
      varying vec2 vUv;
      varying vec3 v_position;

      void main(){
<<<<<<< HEAD

=======
>>>>>>> 31c44301925bf16b37c89c8a67cb64f62d0625bb
        v_position = position;
        vUv = uv;
        vec4 pos = vec4(position, 1.0);
        vec4 modelPosition = modelMatrix * pos;
        vec4 viewPosition = viewMatrix * modelPosition;
        vec4 projectedPosition = projectionMatrix * viewPosition;

        gl_Position = projectedPosition;
      }
    `,
    glsl`
      precision mediump float;

      uniform float uTime;
      uniform sampler2D uTexture;
      uniform float uMouseX;
      uniform float uMouseY;
      uniform float uScreenW;
      uniform float uScreenH;

      varying float uCol;
      varying vec2 vUv;
      varying vec3 v_position;
<<<<<<< HEAD

      float rect(vec2 pt, vec2 size, vec2 center){
        vec2 p = pt-center;
        vec2 halfSize = size;
        float horz = step(-halfSize.x, p.x) - step(halfSize.x, p.x);
        float vert = step(-halfSize.y, p.y) - step(halfSize.y, p.y);
        return horz * vert;
      }
=======
>>>>>>> 31c44301925bf16b37c89c8a67cb64f62d0625bb

      void main() {

        // vec2 screenCoords = gl_FragCoord.xy / vec2(uScreenW, uScreenH);

        // float nScreenW = gl_FragCoord.x / uScreenW;
        // float nScreenH = gl_FragCoord.y / uScreenH;

        // float d = distance(screenCoords, vec2(uMouseX, uMouseY));

        // vec4 textureColor = texture2D(uTexture, vUv);
        // textureColor = vec4(vec3(textureColor.rgb) * d, 1.0);

        // textureColor = vec4(uMouseX, uMouseY, d, 1.0);

        vec3 color = vec3(0.0);

        float inCircle = 1.0 - step(0.9, length(v_position.xy));

        color = vec3(1.0, 1.0, 0.0) * inCircle;

<<<<<<< HEAD
        vec4 textureColor = texture(uTexture, vUv);

        float inRect = rect(v_position.xy, vec2(25.0), vec2(20.0, 20.0));
        vec3 color = vec3(1.0, 1.0, 0.0) * inRect;

        // vec3 test = vec3(textureColor.rgb);
        // vec4 color = vec4(1.0);

        // if(test.r < 0.13){
        //   color = vec4(vec3(textureColor.rgb) + vec3(1.0 - d), 1.0);
        // } else {
        //   color = vec4(vec3(textureColor.rgb) * d, 1.0);
        // }
=======
>>>>>>> 31c44301925bf16b37c89c8a67cb64f62d0625bb

        gl_FragColor = vec4(color, 1.0);
      }
    `,
  )

  extend({ParticleMaterial})