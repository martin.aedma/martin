export const WEBDEV = () => {
    return <div className="project-info-container">
    <h1>WEB DEVELOPER</h1>
    <p>Web technologies are the ones I've used the most. I have prior work experience as front-end developer with React (and Redux). I am also intereseted
        in other frameworks and becoming a full stack developer. For back-end I have built a couple REST API's (PHP/NodeJS).
    </p>
    <h2>PROJECTS</h2>
    <ol>
        <h3>PORTFOLIO</h3>
        <li>
            This site is developed with NextJS. 3D scences and animations are made with React-Three-Fiber (ThreeJS). I also wrote a couple simple GLSL
            material shaders to add some visual effects.
            <br/>
            <p>Source code
                <a href="https://gitlab.com/martin.aedma/martin/-/tree/main/portfolio" target={'_blank'} rel="noopener noreferrer">
                    LINK
                </a>
            </p>
            <img src="/portfolio.svg" />
        </li>
        <h3>BITCOIN DATA</h3>
        <li>
            Scrooge Mc'Ducks Bitcoin market information retrieval web app. NextJS application with API interaction.
            Available to use @
            <a href="https://scrooge.fxjj.fi/" target={'_blank'} rel="noopener noreferrer">
                https://scrooge.fxjj.fi/
            </a>
            <br/>
            <p>Source code
                <a href="https://gitlab.com/martin.aedma/vincit/-/tree/main/mcduck" target={'_blank'} rel="noopener noreferrer">
                    LINK
                </a>
            </p>
            <img src="/bitcoin.svg" />
        </li>
        <h3>REDUX TOOLKIT</h3>
        <li>
        Simple React-Redux example app for learning newest version of Redux - Redux Toolkit.
            <p>Source code
                <a href="https://gitlab.com/martin.aedma/reduxtoolkit" target={'_blank'} rel="noopener noreferrer">
                    LINK
                </a>
            </p>
            <img src="/redux.svg" />
        </li>
        <h3>N2942 XPRESS</h3>
        <li>
            Full stack web application - an online ticket booking system for imaginary railway company.
            This was my very first web application. Front-end is done with old React (Class components), back-end API is made with PHP (Laravael)
            using MySQL database. App was hosted on a Digital Ocean droplet / cloud server.
            <p>App Demo
                <a href="https://www.youtube.com/watch?v=DAkRtWAfDwQ&feature=youtu.be" target={'_blank'} rel="noopener noreferrer">
                    LINK
                </a>
            </p>
            <p>Documentation, code @
                <a href="https://gitlab.com/martin.aedma/n2942-xpress-full-stack-web-app" target={'_blank'} rel="noopener noreferrer">
                    LINK
                </a>
            </p>
            <img src="/n2942.svg" />
        </li>
    </ol>
    <h2>WORKING ON</h2>
    <ol>
        <h3 style={{textAlign: 'center'}}>TYPESCRIPT</h3>
        <h3 style={{textAlign: 'center'}}>BACK-END</h3>
        <h3 style={{textAlign: 'center'}}>FULL STACK DEVELOPMENT</h3>
        <img src="/typescript.svg"/>
    </ol>
</div>
}

export const CSHARP = () => {
    return <div className="project-info-container">
    <h1>C#</h1>
    <p>I've used C# in a few school projects and I quite enjoyed programming with it</p>
    <h2>PROJECTS</h2>
    <ol>
        <h3>RAYCASTER</h3>
        <li>
            Desktop application allowing user to draw, save, load and use custom maps made with lines. User can then move around with arrow
            keyes on that map and algorithm draws line of sight. Algorithm works by casting rays from user location torwards each end point of
            a line. SQLite database is used for storing / loading maps.
            <br/>
            <p>App Demo
                <a href="https://www.youtube.com/watch?v=vQf141AZHdM" target={'_blank'} rel="noopener noreferrer">
                    LINK
                </a>
            </p>
            <p>Source code
                <a href="https://gitlab.com/martin.aedma/raycaster" target={'_blank'} rel="noopener noreferrer">
                    LINK
                </a>
            </p>
            <img src="/rays.png" />
        </li>
        <h3>MIN-MAX ALGORITHM TICTACTOE</h3>
        <li>
            This was my very first project in school, written for introduction to programming. A console application / game of TicTacToe
            with a very rudimentary UI. However, computer uses Min-Max algorithm to make decisions so it will never lose! :) and if you make a
            mistake it will definitely win. Program gets the job done but it's not optimal. For example, if a win could come in 1, 2 or 3 moves it
            will pick simply the first choice it calculates.
            <br/>
            <p>Source code
                <a href="https://gitlab.com/martin.aedma/tictactoe-minmax" target={'_blank'} rel="noopener noreferrer">
                    LINK
                </a>
            </p>
            <img src="/tictactoe.svg" />
        </li>
        <h3>UNITY GAME</h3>
        <li>
        A simple hack'n'slash style game in 3rd person view. You run around in a small closed space while monsters keep spawing in corners
        of the room. Over time monsters become faster so they will eventually catch you. Your goal is to whack as many of them before you get
        caught. For a special attack, every third hit is a chain lightning attack which bounces from each monster it hits to every other monster in a
        small radius. A special dodge move could be executed with a 3 second cooldown that allows to evade attacks momentarily.
            <p>Source code
                <a href="https://gitlab.com/martin.aedma/unity-engine-game" target={'_blank'} rel="noopener noreferrer">
                    LINK
                </a>
            </p>
            <img src="/unity.jpg" />
        </li>
    </ol>
</div>
}


export const CPLUSPLUS = () => {
    return <div className="project-info-container">
    <h1>C++</h1>
    <p>When I started programming, I was drawn to C++ for powerful performance and lower level nature.
        I began learning C++ for 3D graphics programming with OpenGL. I followed many great tutorials on <a href="www.learnopengl.com"
        target={'_blank'} rel='noopener noreferrer'>
            www.learnopengl.com</a> which I've later used in WEB context with ThreeJS.
    </p>
    <h2>PROJECTS</h2>
    <ol>
        <h3>MANDLEBROT SET IN OPENGL</h3>
        <li>
            Mandlebrot set is a facinating mathemathical phenomenoa. Where a single rather simple looking equation produces
            neverending fractal geometry. You can keep zooming in on the image produced by this equation indefinitely ... and
            always just find more of same / similar geometry and patters inside. This project plots Mandlebrot set in custom
            fragment shdaer. You can zoom in until you reach the limit of floating point precision where image gets vague and zoom stops.
            <br/>
            <p>App Demo
                <a href="https://www.youtube.com/watch?v=63k-Yrs0vH4" target={'_blank'} rel="noopener noreferrer">
                    LINK
                </a>
            </p>
            <p>Source code
                <a href="https://gitlab.com/martin.aedma/mandlebrot-opengl" target={'_blank'} rel="noopener noreferrer">
                    LINK
                </a>
            </p>
            <img src="/mandlebrot.jpg" />
        </li>
        <h3>CONCURRENCY IN C++</h3>
        <li>
            One of the advanced programming courses I took was in concurrent programming with C++. This repository contains several of my homework
            solutions. We went over topics like
            <ul>
                <br/>
                <li>Dedlock</li>
                <li>Mutex</li>
                <li>Conditional variable</li>
                <li>Atomics</li>
                <li>Future / Promise</li>
                <li>Async operations</li>
            </ul>
            <br/>
            <p>Source code
                <a href="https://gitlab.com/martin.aedma/concurrent-programming-in-cplusplus" target={'_blank'} rel="noopener noreferrer">
                    LINK
                </a>
            </p>
            <img src="/concurrency.png" />
        </li>
    </ol>
</div>
}